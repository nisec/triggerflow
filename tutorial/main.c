#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
// does a mathematical operation susceptible to timing leak
uint64_t math_function(uint64_t parameter) {
	// TRIGGERFLOW_POI
	if(parameter > UINT_MAX / 2) {
		return parameter;
	} else {
		return pow(parameter, 10);
	}
}
// performes complex cryptographic operation
uint64_t crypto_function(uint64_t parameter) {
	return math_function(parameter);
}
int main(int argc, char **argv) {
	uint64_t public_key = 567;
	uint64_t private_key = 456;
	if(strcmp(argv[1], "verify") == 0) {
		printf("%u\n", crypto_function(public_key)); // TRIGGERFLOW_IGNORE
	} else if(strcmp(argv[1], "sign") == 0) {
		printf("%u\n", crypto_function(private_key));
	}
}
