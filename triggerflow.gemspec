Gem::Specification.new do |s|
  s.name        = 'triggerflow'
  s.version     = '1.3.1'
  s.date        = '2019-08-20'
  s.summary     = "execution inspection tool"
  s.description = ""
  s.authors     = ["Iaroslav Gridin"]
  s.email       = 'voker57@gmail.com'
  s.executables = ["triggerflow"]
  s.homepage    =
    'https://gitlab.com/nisec/triggerflow'
  s.license       = 'MIT'
  s.add_runtime_dependency 'gdb-ruby', '~> 2.2'
end
