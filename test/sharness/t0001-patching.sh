#!/bin/sh

test_description="Test breakpoints and ignores"

. ./lib/test-lib.sh
. ./lib/sharness/sharness.sh

TRIGGERSRB="../../../bin/triggerflow"

triggers() {
	echo "$1" > triggers.txt
}

test_expect_success "Run code that contains annotations in the patches" "
	triggers 'debug ./testapp' &&
	echo 'Applying patch patches/annotation.patch
patching file src/test.c

Now at patch patches/annotation.patch
Debugging: ./testapp
Breakpoint hit at main() test.c:5
	#0 main() test.c:5

Removing patch patches/annotation.patch
Restoring src/test.c

No patches applied' > expected.txt &&
	test_expect_code 64 $TRIGGERSRB ../../patchtest/test.yml triggers.txt > output.txt &&
	test_cmp expected.txt output.txt
"
test_done
