#!/bin/sh

test_description="Test breakpoints and ignores"

. ./lib/test-lib.sh
. ./lib/sharness/sharness.sh

TRIGGERSRB="ruby -W0 ../../../bin/triggerflow"
export LD_LIBRARY_PATH="."
triggers() {
	echo "$1" > triggers.txt
}

test_expect_success "Run a program that breaks" "
	triggers 'debug ./testapp breaksimple' &&
	echo 'Debugging: ./testapp breaksimple
Breakpoint hit at main() testapp.c:24
	#0 main() testapp.c:24
' > expected.txt &&
	test_expect_code 64 $TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt &&
	test_cmp expected.txt output.txt
"

test_expect_success "Run a program that breaks via local PATH" "
	triggers 'debug testapp breaksimple' &&
	echo 'Debugging: testapp breaksimple
Breakpoint hit at main() testapp.c:24
	#0 main() testapp.c:24
' > expected.txt &&
	test_expect_code 64 $TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt &&
	test_cmp expected.txt output.txt
"

test_expect_success "Run a program that has a breakpoint but does not break" "
	triggers 'debug ./testapp none' &&
	echo 'Debugging: ./testapp none' > expected.txt &&
	$TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt &&
	test_cmp expected.txt output.txt
"

test_expect_success "Run a program that has an ignored breakpoint" "
	triggers 'debug ./testapp ignored' &&
	echo 'Debugging: ./testapp ignored' > expected.txt &&
	$TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt &&
	test_cmp expected.txt output.txt
"

test_expect_success "Run a program that has an group-half-ignored breakpoint" "
	triggers 'debug ./testapp halfignored' &&
	echo 'Debugging: ./testapp halfignored
Breakpoint hit at farop() testapp2.c:5
	#0 farop() testapp2.c:5
	#1 main() testapp.c:28
' > expected.txt &&
	test_expect_code 64 $TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt &&
	test_cmp expected.txt output.txt
"

test_expect_success "Run a program that has an group-ignored breakpoint" "
	triggers 'debug ./testapp groupignored' &&
	echo 'Debugging: ./testapp groupignored' > expected.txt &&
	$TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt &&
	test_cmp expected.txt output.txt
"

test_expect_success "Run a program with complicated case of group-ignored lines" "
	triggers 'debug ./testapp groupstuff' &&
	echo 'Debugging: ./testapp groupstuff' > expected.txt &&
	$TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt &&
	test_cmp expected.txt output.txt
"

test_expect_success "Run a program that has two conditional breakpoint, and one of them triggers" "
	triggers 'debug ./testapp condignored' &&
	echo 'Debugging: ./testapp condignored
Breakpoint hit at main() testapp.c:33
	#0 main() testapp.c:33
' > expected.txt &&
	test_expect_code 64 $TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt &&
	test_cmp expected.txt output.txt
"

test_expect_success "Run a program that returns non-zero" "
	triggers 'debug ./testapp fail' &&
	echo 'Debugging: ./testapp fail' > expected.txt &&
	echo 'Building...
./testapp fail returned 10
error: One of the commands failed' > expected_err.txt &&
	test_expect_code 32 $TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt 2> err.txt &&
	test_cmp expected.txt output.txt &&
	test_cmp expected_err.txt err.txt
"

test_expect_success "Run a program that breaks and expect a .dot file with at least line we match" "
	triggers 'debug ./testapp breaksimple' &&
	echo 'Debugging: ./testapp breaksimple
Breakpoint hit at main() testapp.c:24
	#0 main() testapp.c:24
' > expected.txt &&
	test_expect_code 64 $TRIGGERSRB ../../testapp/test.yml triggers.txt --dot test.dot > output.txt &&
	grep -q testapp.c:24 test.dot
"

test_expect_success "Run a program that segfaults" "
	triggers 'debug ./testapp breakself' &&
	test_expect_code 32 $TRIGGERSRB ../../testapp/test.yml triggers.txt > output.txt 2> errors.txt &&
	grep -q 'returned SIGSEGV' errors.txt
"

test_expect_success "Substitute GDB with echo via 'gdb' option in config" "
	echo 'gdb: \"echo this_message_was_appended > tell.txt; gdb\"\nbuild: \"echo 321\"' > triggerflow.yml &&
	echo 'debug echo' > triggers.sh &&
	$TRIGGERSRB triggerflow.yml triggers.sh > output.txt &&
	grep -q this_message_was_appended tell.txt
"

test_done
