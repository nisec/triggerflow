#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
void farop();
void veryfarop(); /* TRIGGERFLOW_IGNORE_GROUP GRP3 */ /* TRIGGERFLOW_IGNORE_GROUP GRP1 */

void noop() {
	// Not a noop to let breakpoint take hold
	printf("...\n"); 
}

void grpop() {
	noop(); /* TRIGGERFLOW_IGNORE_GROUP GRP2 */ /* TRIGGERFLOW_IGNORE_GROUP GRP3 */ /* TRIGGERFLOW_POI */
}

int main(int argc, char**argv) {
	if(argc != 2) {
		printf("Wrong number of arguments\n");
		exit(-5); 
	}
#define ARGCASE(str) if (argv[1] && strcmp(argv[1], str) == 0)
	ARGCASE("breaksimple") {
		/* TRIGGERFLOW_POI */ /* TRIGGERFLOW_IGNORE_GROUP halfignore */ noop();
	} else ARGCASE("ignored") {
		/* TRIGGERFLOW_IGNORE */ farop();
	} else ARGCASE("halfignored") {
		/* TRIGGERFLOW_IGNORE_GROUP halfignore */ farop();
	} else ARGCASE("groupignored") {
		/* TRIGGERFLOW_IGNORE_GROUP groupignore */ veryfarop();
	} else ARGCASE("condignored") {
		int a = 0xdeadf00d;
		/* TRIGGERFLOW_POI_IF a == 0xdeadf00d */ noop();
		/* TRIGGERFLOW_POI_IF a == 0xbaadf00d */ noop();
	} else ARGCASE("groupstuff") {
		/* TRIGGERFLOW_IGNORE_GROUP GRP1 */ /* TRIGGERFLOW_IGNORE_GROUP GRP2 */ grpop();
	} else ARGCASE("breakself") {
		raise(SIGSEGV);
	} else ARGCASE("fail") {
		return 10;
	}
}
